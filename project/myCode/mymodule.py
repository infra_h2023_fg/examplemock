from myCode import oswrapper

#This is my super advanced application
#This is what I want to test


def superAdvancedBehaviorDeleteFile(filename):
	#check if file exists and is a file not a directory
	oswrapper.removeFile(filename)

def superAdvancedBehaviorDiskSpace():
	#Should simply run df
	rep = oswrapper.runDiskSpace()

	#oups, might be a small bug
	oswrapper.removeFile('superimportantfileshitmydogatemyTP.txt')


if __name__ == '__main__':
	rm("test.txt")
