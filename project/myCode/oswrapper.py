import subprocess

#A placeholder for methods to mocks. 
#Just as an exemple to simplify things.
#Note that we could leave those implementations in mymodule.py and mock the subprocess methods directly.


def removeFile(fileName):
	print("Deleting file: " + fileName)
	subprocess.call(['rm', fileName])

def runDiskSpace():
	print("Running df")
	rep = subprocess.check_output(['df']).decode()
	return rep
